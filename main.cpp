

#include <cstdlib>
#include<iostream>

#include <fstream>
#include "Timer.cpp"
#include <ctime>
#include<cmath>

enum DFSPOM {Nie,Zna,Pow};// Nieodwiedzony, Znaleziony, Powrotny/Poprzeczny

using namespace std;

template<class TYP>
class kolejprio;

template<class TYP>
class kolejdwu;

///Nie
template<class TYP>
class drzewo;


template<class TYP>
class kopiec;

class grafmacierz;
class graflista;

struct wierzcholek;
struct wierzcholek2;
struct krawendz;
struct krawendz2;

bool DFS(graflista* graf);
void DFS2(wierzcholek* wierzch);

bool DFS(grafmacierz* graf);
void DFS2(grafmacierz* graf, wierzcholek2* wierzch);

bool BFS(graflista* graf);
void BFS2(wierzcholek* wierzch);

bool BFS(grafmacierz* graf);
void BFS2(grafmacierz* graf, wierzcholek2* wierzch);

//Klasa wenzla, elementu listy i kolejki dwukierunkowej
template <class TYP>
class wenz{
    private:
        unsigned prio;  //klucz
        TYP kwa;		//warto�� w liscie
        wenz<TYP>* nast=NULL;
        wenz<TYP>* poprz=NULL;
    public:
        wenz(TYP aba, int pro=0, wenz<TYP>*pop=NULL){
            kwa=aba;
            poprz=pop;
            prio=pro;
        }
        TYP pob(){
            return kwa;
        }
        TYP* pobw(){
            return &kwa;
        }
        void ust(TYP aba){ //ustawia warto�� na zadana
            kwa=aba;
        }
        ~wenz(){
        }
        void nas(wenz <TYP>* aba){
            nast=aba;
        }
        wenz <TYP>* naw(){
            return nast;
        }
        void pop(wenz <TYP>* aba){
            poprz=aba;
        }
        wenz <TYP>* popw(){
            return poprz;
        }
        friend kolejprio<TYP>; //kolejka prioretytowa
        friend kolejdwu<TYP>;  //lista dwukierunkowa
        friend grafmacierz;
        friend bool DFS(graflista* graf);
        friend bool DFS(grafmacierz* graf);
        friend void DFS2(wierzcholek* wierzch);
        friend void DFS2(grafmacierz* graf,wierzcholek2* wierzch);
        //friend drzewo<wierzcholek>* Kruskal(graflista* graf);
        friend bool BFS(graflista* graf);
        friend void BFS2(wierzcholek* wierzch);
        friend bool BFS(grafmacierz* graf);
        friend void BFS2(grafmacierz* graf, wierzcholek2* wierzch);
};

template <class TYP>
class kolejdwu{
    protected:
        wenz <TYP>* pocz;
        wenz <TYP>* ogo;
    public:
        kolejdwu(){
            pocz=NULL;
            ogo=NULL;
        }
        virtual ~kolejdwu(){
            this->usw();
        }
        bool empty(){
            if(pocz==NULL){
                return true;
            }
            else return false;
        }
        void dop(TYP aba, unsigned pro=0){ //dopisuje element na poczatku listy
            if(empty()){
                ogo=new wenz<TYP>(aba,pro);
                pocz=ogo;
            }
            else{
                if(pocz==ogo){
                    pocz=new wenz<TYP>(aba,pro);
                    pocz->nast=ogo;
                    ogo->poprz=pocz;
                }
                else{
                    wenz<TYP>* pom=pocz;
                    pocz=new  wenz<TYP>(aba,pro);
                    pocz->nast=pom;
                    pom->poprz=pocz;
                }
            }
        }
        int ile(){
            int licz=0;
            wenz <TYP>* pom=pocz;
            for(;pom!=NULL;pom=pom->nast){
                licz++;
            }
            return licz;
        }
        void wys(){ //wypisuje zawartosc listy
            if(empty()){
                //string zxc="Brak element�w w kolejce";
                //throw zxc;
                cout<<"No NIMA"<<endl;
            }
            else{
                wenz <TYP>* pom=pocz;
                for(;pom!=NULL;pom=pom->nast){
                    //cout<<pom->kwa<<" ";
                    if(pom->kwa!=NULL){
                        cout<<pom->kwa->nazwa<<" Nr: "<<pom->kwa->prio<<" STAN: "<<pom->kwa->stan<<endl;
                    }
                }
                cout<<endl;
            }
        }
        TYP usp(){ //usuwa pirwszy element listy
            if(empty()){
                string zxc="Brak element�w w kolejce";
                throw zxc;
            }
            else{
                TYP pomtyp;
                wenz <TYP>* pom=pocz->nast;
                pomtyp=pocz->kwa;
                delete pocz;
                pocz=pom;
                (pocz==NULL) ? ogo=pocz=NULL : pocz->poprz=NULL;
                return pomtyp;
            }
        }
        void usw(){ //usuwa zawartosc listy
            for(wenz <TYP>* pom=pocz;pom!=NULL;pom=pocz){
                usp();
            }
        }
        void doogo(TYP aba, unsigned pro){ //dodaje element na koniec listy
            if(empty()){
                ogo=new wenz<TYP>(aba,pro);
                pocz=ogo;
            }
            else{
                if(pocz==ogo){
                    ogo=new wenz<TYP>(aba,pro,ogo);
                    pocz->nast=ogo;
                }
                else{
                    ogo->nast=new wenz<TYP>(aba,pro,ogo);
                    ogo=ogo->nast;
                }
            }
        }
};

//kolejka priorytetowa
template <class TYP>
class kolejprio : public kolejdwu<TYP>
{
    private:
        using kolejdwu<TYP>::pocz;
        using kolejdwu<TYP>::ogo;
    public:
        wenz<TYP>* poczo(){
            return pocz;
        }
        TYP usuklucz(unsigned pro){
            bool niema=true;
            TYP szukane;
            wenz<TYP>* pom=pocz;
            while((pom!=NULL)&&niema){
                if(pom->prio==pro){
                    niema=false;
                    szukane=pom->pob();///////////////////////////////////////////////////////////////////////???
                        if(pom->poprz!=NULL){
                            wenz<TYP>* pom2=pom->nast;
                            pom=pom->poprz;
                            delete pom->nast;
                            pom->nast=pom2;
                            if(pom2!=NULL) pom2->poprz=pom;
                        }
                        else{
                            if(pom->nast!=NULL){
                                pocz=pom->nast;
                                if(pom->nast!=NULL)	pom->nast->poprz=NULL;
                                delete pom;
                            }
                            else{
                                delete pocz;
                                pocz=ogo=NULL;
                            }
                        }
                }
                else{
                    pom=pom->nast;
                }
            }
            if(niema){
                szukane=NULL;
                //string zxc="Brak elementu o podanym kluczu";
                //throw zxc;
            }
            return szukane;
        }
        TYP szukaklucz(unsigned pro){
            bool niema=true;
            TYP szukane;
            wenz<TYP>* pom=pocz;
            while((pom!=NULL)&&niema){
                if(pom->prio==pro){
                    niema=false;
                    szukane=pom->pob();
                }
                else{
                    pom=pom->nast;
                }
            }
            if(niema){
                //string zxc="Brak elementu o podanym kluczu";
                //throw zxc;
                szukane=NULL;
            }
            return szukane;
        }
        TYP* szukakluczyk(unsigned pro){
            bool niema=true;
            TYP* szukane;
            wenz<TYP>* pom=pocz;
            while((pom!=NULL)&&niema){
                if(pom->prio==pro){
                    niema=false;
                    szukane=pom->pobw();
                }
                else{
                    pom=pom->nast;
                }
            }
            if(niema){
                //string zxc="Brak elementu o podanym kluczu";
                //throw zxc;
                szukane=NULL;
            }
            return szukane;
        }
        void pokluczu(TYP aba,unsigned pro, unsigned para=0){//dodaje element po elemencie o zadanym kluczu pro; para - klucz nowego elementu
            bool niema=true;
            wenz<TYP>* pom=pocz;
            while(pom!=NULL){
                if(pom->prio==pro){
                    niema=false;
                    if(pom->nast!=NULL){
                            wenz<TYP>*pom2=pom->nast;
                            pom->nast=new wenz<TYP>(aba,para);
                            pom->nast->poprz=pom;
                            pom=pom->nast;
                            pom->nast=pom2;
                    }
                    else{
                            pom->nast=new wenz<TYP>(aba,para);
                            pom->nast->poprz=pom;
                            pom=pom->nast;
                            pom->nast=NULL;
                            ogo=pom;
                            pom=NULL;
                    }
                }
                else{
                    pom=pom->nast;
                }
            }
            if(niema){
                string zxc="Brak elementu o podanym kluczu";
                throw zxc;
            }
        }
        TYP usumin(){ // usuwa element o najmniejszym kluczu
            if(pocz==NULL){
                string zxc="Brak element�w w drzewie";
                throw zxc;
            }
            else{
                TYP pomtyp;
                wenz<TYP>* nprio=pocz;
                wenz<TYP>* pom=pocz;
                while(pom!=NULL){
                    if((pom->prio)<(nprio->prio)){
                        nprio=pom;
                    }
                    pom=pom->nast;
                }
                pomtyp=nprio->kwa;
                pom=nprio->nast;
                if(pom!=NULL){
                    if(nprio->poprz!=NULL){
                        pom->poprz=nprio->poprz;
                        pom=nprio->poprz;
                        pom->nast=nprio->nast;
                        delete nprio;
                    }
                    else{
                        pocz=pom;
                        delete pom->poprz;
                        pom->poprz=NULL;
                    }
                }
                else{
                    if(nprio->poprz!=NULL){
                        nprio->poprz->nast=NULL;
                        ogo=nprio->poprz;
                        delete nprio;
                    }
                    else{
                        delete nprio;
                        ogo=pocz=NULL;
                    }
                }
                return pomtyp;
            }
        }
};
// do grafu na lisie
struct krawendz{
    unsigned prio;
    unsigned nazwa;//waga
    wierzcholek* jeden;
    wierzcholek* dwa;
    kolejprio<krawendz*>* kraw1;
    kolejprio<krawendz*>* kraw2;
    DFSPOM stan=Nie;// Niedowiedzony
};

struct wierzcholek{
    unsigned prio;
    string nazwa;
    kolejprio<krawendz*> kraw;
    DFSPOM stan=Nie;// Niedowiedzony
};

//do grafu na macierzy
struct wierzcholek2{
    unsigned prio;
    string nazwa;
    DFSPOM stan=Nie;// Niedowiedzony
};

struct krawendz2{
    unsigned prio;
    unsigned nazwa;
    wierzcholek2* jeden;
    wierzcholek2* dwa;
    DFSPOM stan=Nie;// Niedowiedzony
};

class graflista{
    wierzcholek** wierz;
    krawendz** kraw;
    unsigned liczwierz=0;
    unsigned liczkraw=0;
    unsigned rozm;
    public:
        graflista(unsigned roz){
            rozm=roz;
            unsigned pom=((roz-1)*roz)/2;
            kraw=new krawendz*[pom];
            wierz=new wierzcholek*[rozm];
            for(unsigned i=0;i<pom;i++){
                kraw[i]=NULL;
            }
        }
        ~graflista(){
            for(unsigned i=1; i<rozm; i++)
            {
                delete wierz[i];
            }
            unsigned pom=rozm;
            for(unsigned i=1; i<pom; i++)
            {
                delete kraw[i];
            }
            delete []kraw;
            delete []wierz;
        }
        void dodwierz(string nazwa){
            wierzcholek* pom=new wierzcholek;
            pom->nazwa=nazwa;
            pom->prio=liczwierz;
            wierz[liczwierz]=pom;
            liczwierz++;
        }
        void dodkraw(unsigned nazwa,unsigned jeden,unsigned dwa){
            krawendz* pom=new krawendz;
            pom->nazwa=nazwa;
            pom->prio=liczkraw;
            pom->jeden=wierz[jeden];
            pom->dwa=wierz[dwa];
            if((pom->jeden!=NULL)&&(pom->dwa!=NULL)){
                wierz[jeden]->kraw.dop(pom,liczkraw);
                wierz[dwa]->kraw.dop(pom,liczkraw);
                pom->kraw1=&pom->jeden->kraw;//
                pom->kraw2=&pom->dwa->kraw;//
                kraw[liczkraw]=pom;
                liczkraw++;
            }
            else{
                cout<<"Brak wierzcholka lub wierzcholkow"<<endl;
                delete pom;
            }

        }
        krawendz usukraw(unsigned prio){///
            krawendz* pom=kraw[prio];
            krawendz zwrot;
            if(pom!=NULL){
                pom->jeden->kraw.usuklucz(prio);
                pom->dwa->kraw.usuklucz(prio);
                zwrot=*pom;
                delete kraw[prio];
                kraw[prio]=NULL;
                delete pom;
                return zwrot;
            }
            else{
                cout<<"NO NIMA"<<endl;
            }
        }
        wierzcholek usuwierz(unsigned prio){////??????
            wierzcholek* pom=wierz[prio];
            if(pom!=NULL){
                wierzcholek zwrot;
                unsigned pomprio;

                while(!pom->kraw.empty()){
                    pomprio=pom->kraw.usp()->prio;
                    delete kraw[prio];
                    kraw[prio]=NULL;
                }
                zwrot=*pom;
                wierz[prio]=NULL;
                delete pom;
                return zwrot;
            }
            else{
                cout<<"NO NIMA"<<endl;
            }
        }
        /*void wyskraw(){
            kraw.wys();
        }
        void wyswierz(){
            wierz.wys();
        }
        void wyspolacz(unsigned prio){
            wierz.szukaklucz(prio)->kraw.wys();
        }*/
        void zastwierz(unsigned prio,string nazwa){
            wierzcholek* pom=wierz[prio];
            if(pom!=NULL){
                pom->nazwa=nazwa;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        void zastkraw(unsigned prio, unsigned nazwa){
            krawendz* pom=kraw[prio];
            if(pom!=NULL){
                pom->nazwa=nazwa;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        void krawwierzch(unsigned prio){
            krawendz* pom=kraw[prio];
            if(pom!=NULL){
                cout<<pom->jeden->nazwa<<" "<<pom->jeden->prio<<endl;
                cout<<pom->dwa->nazwa<<" "<<pom->dwa->prio<<endl;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        wierzcholek* przeciwlegly(unsigned prio,unsigned wierzch){
            krawendz* pom=kraw[prio];
            if(pom!=NULL){
                if(pom->dwa->prio==wierzch){
                    return pom->jeden;
                }
                else{
                    if(pom->jeden->prio==wierzch){
                        return pom->dwa;
                    }
                    else{
                        cout<<"NIE PASUJE KRAWEDZ DO WIERZCHOLKA"<<endl;
                    }
                }
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        bool czysasiednie(unsigned prio,unsigned szukane){
            wierzcholek* pom=wierz[prio];
            if(pom!=NULL){
                wierzcholek* pom2=wierz[prio];
                if(pom2!=NULL){
                    unsigned i=0;
                    bool znalaz=false;
                    krawendz* pomk=pom->kraw.szukaklucz(i);
                    while((pomk!=NULL)&&(!znalaz)){
                        if((pomk->dwa==pom2)||(pomk->jeden==pom2)){
                            znalaz=true;
                        }
                        pomk=pom->kraw.szukaklucz(i);
                        i++;
                    }
                    if(!znalaz){
                        return false;
                        cout<<"NIE SASIADUJA"<<endl;
                    }
                    else{
                        return true;
                        cout<<"SASIADUJA"<<endl;
                    }
                }
            }
            else{
                return false;
                cout<<"NO NIE MA"<<endl;
            }
        }
        friend bool DFS(graflista* graf);
        friend bool DFS(wierzcholek wierzch);
        //friend drzewo<wierzcholek>* Kruskal(graflista* graf);
        friend bool BFS(graflista* graf);

};

//graf na macierzy

class grafmacierz{
    wierzcholek2** wierz;
    krawendz2** kraw;
    krawendz2*** macierz;
    unsigned liczwierz=0;
    unsigned liczkraw=0;
    unsigned rozm;
    public:
        grafmacierz(unsigned roz){
            rozm=roz;
            wierz=new wierzcholek2*[rozm];
            unsigned pom=(roz*(roz-1))/2;
            kraw=new krawendz2*[pom];
            macierz=new krawendz2**[roz];
            for(unsigned i=0;i<(rozm);i++){
                macierz[i]=new krawendz2* [roz];
            }
            for(unsigned i=0;i<(rozm);i++){
                for(unsigned j=0;j<rozm;j++){
                    macierz[i][j]=NULL;
                }
            }
            for(unsigned i=0;i<pom;i++){
                kraw[i]=NULL;
            }
        }
        ~grafmacierz(){
            unsigned pom=(rozm*(rozm-1))/2;
            krawendz2* pomk;
            wierzcholek2* pomw;
            for(unsigned i=0;i<(pom);i++){
                pomk=kraw[i];
                if(pomk!=NULL) delete pomk;
            }
            for(unsigned i=0;i<rozm;i++){
                pomw=wierz[i];
                if(pomw!=NULL) delete pomw;
            }
            for(unsigned i=0;i<rozm;i++){
                delete [] macierz[i];
            }
            delete [] macierz;
        }
        void dodwierz(string nazwa){//////////////
            wierzcholek2* pom=new wierzcholek2;
            pom->nazwa=nazwa;
            pom->prio=liczwierz;
            wierz[liczwierz]=pom;
            liczwierz++;
        }
        void dodkraw(unsigned nazwa,unsigned jeden,unsigned dwa){
            krawendz2* pom=new krawendz2;
            pom->nazwa=nazwa;
            pom->jeden=wierz[jeden];
            pom->dwa=wierz[dwa];
            pom->prio=liczkraw;
            macierz[jeden][dwa]=pom;
            macierz[dwa][jeden]=pom;
            kraw[liczkraw]=pom;
            pom=NULL;
            liczkraw++;

        }
        krawendz2 usukraw(unsigned prio){
            krawendz2* pom=kraw[prio];
            krawendz2 zwrot;
            if(pom!=NULL){
                unsigned pom1,pom2;
                pom1=pom->dwa->prio;
                pom2=pom->jeden->prio;
                macierz[pom1][pom2]=NULL;
                macierz[pom2][pom1]=NULL;
                zwrot=*pom;
                kraw[prio]=NULL;
                delete pom;
                return zwrot;
            }
            else{
                cout<<"NO NIMA"<<endl;
            }
        }
        wierzcholek2 usuwierz(unsigned prio){////////
            wierzcholek2* pom=wierz[prio];
            if(pom!=NULL){
                wierzcholek2 zwrot;
                unsigned i=0;
                unsigned pomun=0;
                krawendz2** pomkol=macierz[prio];
                while(i<liczwierz){//////
                    if(pomkol[i]!=NULL){
                        pomun=pomkol[i]->prio;
                        usukraw(pomun);
                    }
                    i++;
                }

                zwrot=*pom;
                delete wierz[prio];
                wierz[prio]=NULL;
                //wyswierz();
                delete pom;
                return zwrot;
            }
            else{
                cout<<"NO NIMA"<<endl;
            }
        }
    /*	void wyskraw(){////Nie
            kraw.wys();
        }
        void wyswierz(){///Nie
            wierz.wys();
        }*/
        void zastwierz(unsigned prio,string nazwa){
            wierzcholek2* pom=wierz[prio];
            if(pom!=NULL){
                pom->nazwa=nazwa;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        void zastkraw(unsigned prio, unsigned nazwa){
            krawendz2* pom=kraw[prio];
            if(pom!=NULL){
                pom->nazwa=nazwa;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
    /*	void wyspolacz(unsigned prio){//Nie
            if(macierz[prio]!=NULL){
                macierz->wys();
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }*/
        void krawwierzch(unsigned prio){
            krawendz2* pom=kraw[prio];
            if(pom!=NULL){
                cout<<pom->jeden->nazwa<<" "<<pom->jeden->prio<<endl;
                cout<<pom->dwa->nazwa<<" "<<pom->dwa->prio<<endl;
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        void przeciwlegly(unsigned prio,unsigned wierzch){
            krawendz2* pom=kraw[prio];
            if(pom!=NULL){
                if(pom->dwa->prio==wierzch){
                    cout<<pom->jeden->nazwa<<" "<<pom->jeden->prio<<endl;
                }
                else{
                    if(pom->jeden->prio==wierzch){
                        cout<<pom->dwa->nazwa<<" "<<pom->dwa->prio<<endl;
                    }
                    else{
                        cout<<"NIE PASUKE KRAWEDZ DO WIERZCHOLKA"<<endl;
                    }
                }
            }
            else{
                cout<<"NO NIE MA"<<endl;
            }
        }
        bool czysasiednie(unsigned prio,unsigned szukane){/////////////////
            wierzcholek2* pom=wierz[prio];
            if(pom!=NULL){
                wierzcholek2* pom2=wierz[szukane];
                if(pom2!=NULL){
                    bool znalaz=false;
                    if(macierz[prio][szukane]!=NULL) znalaz=true;
                    if(!znalaz){
                        return false;
                        //cout<<"NIE SASIADUJA"<<endl;
                    }
                    else{
                        return true;
                        cout<<"SASIADUJA"<<endl;

                    }
                }
            }
            else{
                //cout<<"NO NIE MA"<<endl;
                return false;
            }
            return false;
        }
        void zapisz(string name="PomiaryGraf.txt"){//Nie
            //
        }
    /*	void nah(){
            if(macierz.empty()){
                //string zxc="Brak element�w w kolejce";
                //throw zxc;
                cout<<"No NIMA"<<endl;
            }
            else{
                wenz <kolejprio<krawendz2*>*>* pom=macierz.pocz;
                for(;pom!=NULL;pom=pom->nast){
                    //cout<<pom->kwa<<" ";
                    cout<<"HEJ ";
                }
                cout<<endl;
            }
        }*/
        friend bool DFS(grafmacierz* graf);
        friend void DFS2(grafmacierz* graf, wierzcholek2* wierzch);
        friend bool BFS(grafmacierz* graf);
        friend void BFS2(grafmacierz* graf,wierzcholek2*wierzch);
};

graflista* generujl(graflista* graf,unsigned ilewierzch,unsigned gensto){//gensto 0-4*25%
    if(ilewierzch>2){
        graf=new graflista(ilewierzch);
        string ala="W";
        for(unsigned i=0;i<ilewierzch;i++){
            graf->dodwierz(ala);
        }
        //cout<<"HAN"<<endl;///////
        unsigned i=0;
        int pom1,pom2,pom3;
        srand(time(NULL));
        if(gensto>=4){
            //cout<<"TU"<<endl;
            for(unsigned i=0;i<(ilewierzch-1);i++){
                for(unsigned j=(i+1);j<ilewierzch;j++){
                    pom3=rand()%1000;
                    //cout<<"NAH"<<endl;
                    graf->dodkraw(pom3,i,j);
                }
            }
        }
        else{
            unsigned ilekraw=(ilewierzch*(ilewierzch-1)*gensto)/8;
            while(i<=ilekraw){
                pom1=rand()%ilewierzch;
                pom2=rand()%(ilewierzch-1)+1;
                pom3=rand()%1000;//waga krawendzi
                if(pom1!=pom2){
                    if(!graf->czysasiednie(pom1,pom2)){///
                        graf->dodkraw(pom3,pom1,pom2);
                        i++;
                    }
                }
            }
        }
        //cout<<"SOLO"<<endl;
    }
    //graf->wyswierz();
    //graf->wyskraw();
    return graf;
}

grafmacierz* generujm(grafmacierz* graf,unsigned ilewierzch,unsigned gensto){//
    if(ilewierzch>2){
        graf=new grafmacierz(ilewierzch);
        string ala="W";
        for(unsigned i=0;i<ilewierzch;i++){
            graf->dodwierz(ala);
        }

        unsigned i=0;
        int pom1,pom2,pom3;
        srand(time(NULL));cout<<"MACH"<<endl;//////////////
        if(gensto>=4){
            for(unsigned i=0;i<(ilewierzch);i++){
                for(unsigned j=(i+1);j<ilewierzch;j++){
                    pom3=rand()%5700;
                    graf->dodkraw(pom3,i,j);
                    cout<<j<<endl;
                }
            }
        }
        else{
            unsigned ilekraw=(ilewierzch*(ilewierzch)*gensto)/8;
            while(i<=ilekraw){
                pom1=rand()%ilewierzch;
                pom2=rand()%(ilewierzch-1)+1;
                pom3=rand()%1000;//waga krawendzi
                if(pom1!=pom2){
                    cout<<"BUM"<<pom1<<" "<<pom2<<endl;
                    if(!graf->czysasiednie(pom1,pom2)){
                        cout<<"NAH"<<endl;
                        graf->dodkraw(pom3,pom1,pom2);
                        i++;
                    }
                }
            }
        }
    }
    //graf->wyswierz();
    //graf->wyskraw();

    return graf;
}


void DFS2(wierzcholek* wierzch){ //PRZESZUKIWANIE W G�AB CZESC 2
    wierzch->stan=Zna;
    wenz<krawendz*>* wenz=wierzch->kraw.poczo();
    if(wenz!=NULL){
        krawendz* pom=wenz->kwa;
        while(wenz!=NULL){
            pom=wenz->kwa;
            if(pom!=NULL){
                if(pom->stan==Nie){
                    pom->stan=Zna;
                    if(pom->dwa->prio==wierzch->prio){
                        if(pom->jeden->stan==Nie){
                            pom->stan=Zna;
                            DFS2(pom->jeden);
                        }
                        else{
                            pom->stan=Pow;// cout<<"Pow"<<endl;
                        }
                    }
                    else{
                        if(pom->dwa->stan==Nie){
                            pom->stan=Zna; //cout<<"ZNA"<<endl;
                            DFS2(pom->dwa);
                        }
                        else{
                            pom->stan=Pow; //cout<<"Pow"<<endl;
                        }
                    }
                }
            }
            wenz=wenz->nast;
        }
    }
}


void DFS2(grafmacierz* graf, wierzcholek2* wierzch){
    wierzch->stan=Zna;
    krawendz2* pom;
    wierzch->stan=Zna;
    krawendz2** lista=graf->macierz[wierzch->prio];
    for(unsigned i=0;i<graf->rozm;i++){
        cout<<"PP"<<i<<" "<<graf->rozm<<endl;
        pom=lista[i];
        cout<<"NOM"<<endl;
        if(pom!=NULL){
            cout<<pom->nazwa<<endl;
            cout<<"Helond"<<endl;
            if(pom->stan==Nie){
                    pom->stan=Zna;
                    if(pom->dwa->prio==wierzch->prio){
                        if(pom->jeden->stan==Nie){
                            pom->stan=Zna;cout<<"ZNA1"<<endl;
                            DFS2(graf,pom->jeden);
                        }
                        else{
                            pom->stan=Pow; cout<<"Pow"<<endl;
                        }
                    }
                    else{
                        if(pom->dwa->stan==Nie){
                            pom->stan=Zna; cout<<"ZNA"<<endl;
                            DFS2(graf,pom->dwa);
                        }
                        else{
                            pom->stan=Pow; cout<<"Pow"<<endl;
                        }
                    }
                }
        }
        cout<<"Nie"<<i<<endl;

    }
    /*
    unsigned i=0;
    while(i<graf->liczwierz){
        if(wenz->kwa!=NULL){
            pom=wenz->kwa;
            if(pom->stan==Nie){
                pom->stan=Zna;
                if(pom->dwa->prio==wierzch->prio){
                    if(pom->jeden->stan==Nie){
                        pom->stan=Zna;
                        DFS2(graf,pom->jeden);
                    }
                    else{
                        pom->stan=Pow;// cout<<"Pow"<<endl;
                    }
                }
                else{
                    if(pom->dwa->stan==Nie){
                        pom->stan=Zna; //cout<<"ZNA"<<endl;
                        DFS2(graf,pom->dwa);
                    }
                    else{
                        pom->stan=Pow;// cout<<"Pow"<<endl;
                    }
                }
            }
        }
        wenz=wenz->nast;
        i++;
    }*/
}

bool DFS(graflista* graf){
    for(unsigned i=0;i<graf->rozm;i++){
        graf->wierz[i]->stan=Nie;
    }

    unsigned pom=(graf->rozm*(graf->rozm-1))/2;
    for(unsigned i=0;i<pom;i++){
        if(graf->kraw[i]!=NULL){
            graf->kraw[i]->stan=Nie;
        }
    }///////

    /*
    wenz1=graf->kraw.poczo();/////////////////////////////////////
    while(wenz1!=NULL){
        kraw=wenz1->kwa;
        if(kraw!=NULL) kraw->stan=Nie;
        wenz1=wenz1->nast;
 }
    for (unsigned i=0; i < wierzcholki.size(); i++){
        graf
    }
    wierzcholek* wierzch;
    wenz<wierzcholek*>* wenz2;
    wenz2=graf->wierz.poczo();
    while(wenz2!=NULL){
        wierzch=wenz2->kwa;
        if(wierzch!=NULL) wierzch->stan=Nie;
        wenz2=wenz2->nast;
    }
    //graf.wyskraw();
//	graf.wyswierz();
    //cout<<"DON"<<endl;
    */
    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->rozm;i++){
        wierzcholek* pom;
        pom=graf->wierz[i];
        if(pom!=NULL){
            //////////
            if(pom->stan==Nie){
                DFS2(pom);
                podgrafy++;
            }
        }
    }
    return (podgrafy==1)? true : false;
}

bool DFS(grafmacierz* graf){

    for(unsigned i=0;i<graf->rozm;i++){
        graf->wierz[i]->stan=Nie;
    }
    unsigned pom=(graf->rozm*(graf->rozm-1))/2;
    for(unsigned i=0;i<pom;i++){
        if(graf->kraw[i]!=NULL){
            graf->kraw[i]->stan=Nie;
        }
    }
    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->rozm;i++){
        if(graf->wierz[i]!=NULL){
            if(graf->wierz[i]->stan==Nie){
            DFS2(graf,graf->wierz[i]);
            podgrafy++;
            }
        }
    }
    /*
    krawendz2* kraw;
    wenz<krawendz2*>* wenz1;
    wenz1=graf->kraw.poczo();/////////////////////////////////////
    while(wenz1!=NULL){
        kraw=wenz1->kwa;
        if(kraw!=NULL) kraw->stan=Nie;
        wenz1=wenz1->nast;
    }
    wierzcholek2* wierzch;
    wenz<wierzcholek2*>* wenz2;
    wenz2=graf->wierz.poczo();
    while(wenz2!=NULL){
        wierzch=wenz2->kwa;
        if(wierzch!=NULL) wierzch->stan=Nie;
        wenz2=wenz2->nast;
    }
    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->liczwierz;i++){
        wierzcholek2* pom;
        pom=graf->wierz.szukaklucz(i);
        if(pom!=NULL){
            //////////
            if(pom->stan==Nie){
                DFS2(graf,pom);
                podgrafy++;
            }
        }
    }*/
    return (podgrafy==1)? true : false;
}

void BFS2(wierzcholek* wierzch){
    wierzch->stan=Zna;
    wenz<krawendz*>* wenz=wierzch->kraw.poczo();
    kolejprio<wierzcholek*> wierzcho;
    if(wenz!=NULL){
        krawendz* pom=wenz->kwa;
        while(wenz!=NULL){
            pom=wenz->kwa;
            if(pom!=NULL){
                if(pom->stan==Nie){
                    //pom->stan=Zna;
                    if(pom->dwa->prio==wierzch->prio){
                        if(pom->jeden->stan==Nie){
                            pom->stan=Zna; //cout<<"ZNA"<<endl;
                            pom->jeden->stan=Zna;
                            wierzcho.doogo(pom->jeden,0);
                        }
                        else{
                            pom->stan=Pow;
                        }
                    }
                    else{
                        if(pom->dwa->stan==Nie){
                            pom->stan=Zna; //cout<<"ZNA"<<endl;
                            pom->dwa->stan=Zna;
                            wierzcho.doogo(pom->dwa,0);
                        }
                        else{
                            pom->stan=Pow;
                        }
                    }
                }
            }
            wenz=wenz->nast;
        }
        wierzcholek* pom2;
        while(!wierzcho.empty()){
            pom2=wierzcho.usp();
            BFS2(pom2);
        }
        wierzcho.usw();
    }
}

bool BFS(graflista* graf){
    for(unsigned i=0;i<graf->rozm;i++){
        graf->wierz[i]->stan=Nie;
    }
    unsigned pom=(graf->rozm*(graf->rozm-1))/2;
    for(unsigned i=0;i<pom;i++){
       // cout<<i<<endl;
        if(graf->kraw[i]!=NULL){
        //	cout<<"Nie"<<endl;
            graf->kraw[i]->stan=Nie;
        }
    }
    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->rozm;i++){
        if(graf->wierz[i]!=NULL){
            if(graf->wierz[i]->stan==Nie){
            BFS2(graf->wierz[i]);
            podgrafy++;
            }
        }
    }
    /*
    krawendz* kraw;
    wenz<krawendz*>* wenz1;
    wenz1=graf->kraw.poczo();/////////////////////////////////////
    while(wenz1!=NULL){
        kraw=wenz1->kwa;
        if(kraw!=NULL) kraw->stan=Nie;
        wenz1=wenz1->nast;
    }
    wierzcholek* wierzch;
    wenz<wierzcholek*>* wenz2;
    wenz2=graf->wierz.poczo();
    while(wenz2!=NULL){
        wierzch=wenz2->kwa;
        if(wierzch!=NULL) wierzch->stan=Nie;
        wenz2=wenz2->nast;
    }
    //graf->wyskraw();
    //graf->wyswierz();

    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->rozm;i++){
        wierzcholek* pom;
        pom=graf->wierz[i];
        if(pom!=NULL){
            if(pom->stan==Nie){
                BFS2(pom);
                podgrafy++;
            }
        }
    }*/
    return (podgrafy==1)? true : false;
}

void BFS2(grafmacierz* graf, wierzcholek2* wierzch){
    wierzch->stan=Zna;
    krawendz2** lista=graf->macierz[wierzch->prio];
    kolejprio<wierzcholek2*> wierzcho;
    for(unsigned i=0;i<graf->rozm;i++){
        if(lista[i]!=NULL){
            krawendz2* pom=lista[i];
            if(pom->stan==Nie){
                if(pom->dwa->prio==wierzch->prio){
                    if(pom->jeden->stan==Nie){
                        pom->stan=Zna;
                        pom->jeden->stan=Zna;
                        wierzcho.doogo(pom->jeden,0);
                    }
                    else{
                        pom->stan=Pow;
                    }
                }
                else{
                    if(pom->dwa->stan==Nie){
                        pom->stan=Zna; //cout<<"ZNA"<<endl;
                        pom->dwa->stan=Zna;
                        wierzcho.doogo(pom->dwa,0);
                    }
                    else{
                        pom->stan=Pow;
                    }
                }
            }
        }
    }
    wierzcholek2* pom2;
    while(!wierzcho.empty()){
        pom2=wierzcho.usp();
        BFS2(graf,pom2);
    }
    /*
    krawendz2* pom;
    unsigned i=0;
    while(i<graf->liczwierz){
        if(wenz->kwa!=NULL){
            pom=wenz->kwa;
            if(pom->stan==Nie){
                if(pom->dwa->prio==wierzch->prio){
                    if(pom->jeden->stan==Nie){
                        pom->stan=Zna;
                        pom->jeden->stan=Zna;
                        wierzcho.doogo(pom->jeden,0);
                    }
                    else{
                        pom->stan=Pow;
                    }
                }
                else{
                    if(pom->dwa->stan==Nie){
                        pom->stan=Zna; //cout<<"ZNA"<<endl;
                        pom->dwa->stan=Zna;
                        wierzcho.doogo(pom->dwa,0);
                    }
                    else{
                        pom->stan=Pow;
                    }
                }
            }
        }
        wenz=wenz->nast;
        i++;
    }
    wierzcholek2* pom2;
    while(!wierzcho.empty()){
        pom2=wierzcho.usp();
        BFS2(graf,pom2);
    }*/
}

bool BFS(grafmacierz* graf){
    for(unsigned i=0;i<graf->rozm;i++){
        graf->wierz[i]->stan=Nie;
    }
    unsigned pom=(graf->rozm*(graf->rozm-1))/2;
    for(unsigned i=0;i<pom;i++){
       // cout<<i<<endl;
        if(graf->kraw[i]!=NULL){
        //	cout<<"Nie"<<endl;
            graf->kraw[i]->stan=Nie;
        }
    }
    unsigned podgrafy=0;
    for(unsigned i=0;i<graf->rozm;i++){
        if(graf->wierz[i]!=NULL){
            if(graf->wierz[i]->stan==Nie){
            BFS2(graf,graf->wierz[i]);
            podgrafy++;
            }
        }
    }
    return (podgrafy==1)? true : false;
}



int main(){
    #define ILE 50
    float czassuma;
    unsigned Roz[]={10,50,100,250,500,1000};
    unsigned rozm;

//    grafmacierz* lol;
    graflista* lol;

    //lol=generujl(NULL,7,0);//dla parzy


    //DFS(lol);

    ///test dfs
    for(unsigned i=0;i<6;i++){
        rozm=Roz[i];
        for(unsigned g=0;g<5;g++){
            Timer czas;
            for(unsigned j=0;j<ILE;j++){
//                lol=generujm(NULL,rozm,g);
                lol=generujl(NULL,rozm,g);
                czas.start();
                DFS(lol);
                czas.stop();
                czassuma+=czas.getElapsedTimeInMilliSec();
                delete lol;
            }
            cout<<"TEST DFS: "<<rozm<<" "<<g<<" Czas:"<<czassuma/(ILE*1000)<<"s"<<endl;
            czassuma=0;
        }
    }
  //BFS testy
    for(unsigned i=0;i<6;i++){
        rozm=Roz[i];
        for(unsigned g=0;g<5;g++){
            Timer czas;
            for(unsigned j=0;j<ILE;j++){
//                lol=generujm(NULL,rozm,g);
                lol=generujl(NULL,rozm,g);
                czas.start();
                BFS(lol);
                czas.stop();
                czassuma+=czas.getElapsedTimeInMilliSec();
                delete lol;
            }
            cout<<"TEST BFS: "<<rozm<<" "<<g<<" Czas:"<<czassuma/(ILE*1000)<<"s"<<endl;
            czassuma=0;
        }
    }
    cout<<"END"<<endl;
    return 0;
}
